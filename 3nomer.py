import random
import matplotlib.pyplot as plt
from timeit import default_timer as timer

# Функция умножения вектора на константу
def generation(element, const):
	mass = []
	for i in range(len(element)):
		mass.append(element[i] * const)
	return mass

# Функция сложения векторов
def amount(element_1, element_2):
	mass = []
	for i in range(len(element_1)):
		mass.append(element_1[i] + element_2[i])
	return mass

# Функция создания целочисленного вектора
def random_int(i, lower_bound, upper_bound):
	mass = []
	for n in range(i):
		mass.append(random.randint(lower_bound, upper_bound))
	return mass

# Функция создания вещественного вектора
def random_float(i, lower_bound, upper_bound):
	mass = []
	for n in range(i):
		mass.append(random.uniform(lower_bound, upper_bound))
	return mass

# Генерация константы
const = random.randint(0, 10)

# Создание массивов для построения графика
mass_value, mass_time_int, mass_time_float = [], [], []

# Создание векторов
for i in range(10, 101, 10):
	x_1 = random_int(i, 1, 10)
	y_1 = random_int(i, 1, 10)
	x_2 = random_float(i, 1, 10)
	y_2 = random_float(i, 1, 10)

	mean_val_int, mean_val_float = 0, 0

# Время сложения векторов
	for n in range(3):
		start_time = timer()
		for_int_z = amount(generation(x_1, const), y_1)
		mean_val_int += timer() - start_time

		start_time = timer()
		for_float_z = amount(generation(x_2, const), y_2)
		mean_val_float += timer() - start_time

# Заносим в массивы для построения графика
	mass_value.append(i)
	mass_time_int.append(mean_val_int/3)
	mass_time_float.append(mean_val_float/3)

# Построение графика
fig, ax = plt.subplots()
plt.plot(mass_value, mass_time_int,'c-o', label = 'int')
plt.plot(mass_value, mass_time_float,'m-o', label = 'float')
plt.title('График зависимости роста времени подсчета от длины вектора')
plt.legend(fontsize = '10',
		   facecolor = 'white',
		   edgecolor = 'black',
		   title = 'Прямые',
		   title_fontsize = '10')
ax.set_xlabel('Длина вектора')
ax.set_ylabel('Время (сек)')
plt.grid()
plt.show()