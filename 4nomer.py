import random
import matplotlib.pyplot as plt
from timeit import default_timer as timer

# Функция для перемножения матриц
def generation(matrix_1, matrix_2):
    for i in range(len(matrix_1[0])):
        for j in range(len(matrix_1)):
            matrix_1[i][j] *= matrix_2[i][j]
    return matrix_1

# Функция создания целочисленных матриц
def random_int(var, lower_bound, upper_bound):
    mass = [[1 for _ in range(var)] for _ in range(var)]
    for i in range(var):
        for j in range(var):
            mass[i][j] = random.randint(lower_bound, upper_bound)
    return mass

# Функция создания вещественных матриц
def random_float(var, lower_bound, upper_bound):
    mass = [[1 for _ in range(var)] for _ in range(var)]
    for i in range(var):
        for j in range(var):
            mass[i][j] = random.uniform(lower_bound, upper_bound)
    return mass

# Создание массивов для построения графиков
mass_value, mass_time_int, mass_time_float = [], [], []

# Создание матриц
for i in range(10, 101, 10):
    matrix_int_1 = random_int(i, 1, 10)
    matrix_int_2 = random_int(i, 1, 10)
    matrix_float_1 = random_float(i, 1, 10)
    matrix_float_2 = random_float(i, 1, 10)

    mean_value_int = 0
    mean_value_float = 0

# Время перемножения матриц
    for k in range(3):
        start_time = timer()
        n1 = generation(matrix_int_1, matrix_int_2)
        mean_value_int += timer() - start_time

        start_time = timer()
        n2 = generation(matrix_float_1, matrix_float_2)
        mean_value_float += timer() - start_time

# Заносим в массивы для построения графика
    mass_value.append(i)
    mass_time_int.append(mean_value_int / 3)
    mass_time_float.append(mean_value_float / 3)

# Построение графика
fig, ax = plt.subplots()
plt.plot(mass_value, mass_time_int, 'c-o', label='int')
plt.plot(mass_value, mass_time_float, 'm-o', label='float')
plt.title('График зависимости времени подсчета от размерности матрицы')
plt.legend(fontsize='10',
           facecolor='white',
           edgecolor='black',
           title='Прямые',
           title_fontsize='10')
ax.set_xlabel('Размерность матрицы')
ax.set_ylabel('Время (сек)')
plt.grid()
plt.show()
