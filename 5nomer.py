import copy
import random
from sys import getsizeof
import time


# Функция нахождения определителя матрицы
def determinant(matrix, n):
    if n == 1:
        return (matrix[0][0])
    elif n == 2:
        return (matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1])
    else:
        s = 0
        for i in range(n):
            matrix_1 = copy.deepcopy(matrix)
            matrix_1.pop(0)
            for j in range(n - 1):
                matrix_1[j].pop(i)
            s += (-1) ** i * matrix[0][i] * determinant(matrix_1, n - 1)
        return s


# Функция для решения системы уравнений методом Крамера
def method_cramer(coefficients, free_coefficients, num, d):
    value = []
    for i in range(num):  # Для каждой переменной
        line = copy.deepcopy(coefficients)
        for j in range(num):
            # Заменяем столбец с коэффициентами при переменной на свободные
            line[j][i] = free_coefficients[j]
        # Находим определитель матрицы с заменёнными коэффициентами
        line_det = determinant(line, num)
        value.append(line_det / d)
    return value


# Функция для решения системы уравнений методом Гаусса
def method_gauss(matrix, num):
    for i in range(0, num):
        maximum_element = abs(matrix[i][i])
        maximum_range = i
        for j in range(i + 1, num):
            if abs(matrix[j][i]) > maximum_element:
                maximum_element = abs(matrix[j][i])
                maximum_range = j

        for j in range(i, num + 1):
            matrix[maximum_range][j], matrix[i][j] = matrix[i][j], matrix[maximum_range][j]

        for j in range(i + 1, num):
            result = - (matrix[j][i] / matrix[i][i])
            for k in range(i, num + 1):
                if i == k:
                    matrix[j][k] = 0
                else:
                    matrix[j][k] += result * matrix[i][k]

    solution = [0 for elem in range(num)]
    for i in range(num - 1, -1, -1):
        solution[i] = matrix[i][num] / matrix[i][i]
        for j in range(i - 1, -1, -1):
            matrix[j][num] -= matrix[j][i] * solution[i]
    return solution


# Создание массивов для хранения данных
coefficients, free_coefficients = [], []

# Размерность системы уравнений
number = int(input('Сколько уравнений? '))

for i in range(number):
    coefficients.append([])
    for j in range(number):
        # Записываем в массив коэффициенты
        coefficients[i].append(random.randint(-10, 10))
    # Записываем в массив свободные коэффициенты
    free_coefficients.append(random.randint(-10, 10))

# Определитель матрицы
det = determinant(coefficients, number)

# Запись времени решения системы уравнений
# и объема занимаемой памяти в файл (метод Крамера)
if det == 0:
    print("Определитель равен нулю")
else:
    start_time = time.time()
    result_cramer = method_cramer(coefficients, free_coefficients, number, det)
    with open('time_cramer.txt', 'w') as file:
        file.write(str(time.time() - start_time) + '\n')
    with open('memory_cramer.txt', 'w') as file:
        file.write(str(getsizeof(result_cramer)) + '\n')
    print('Ответ уравнений методом Cramer:', result_cramer)

# Запись времени решения системы уравнений
# и объема занимаемой памяти в файл (метод Гаусса)
matrix = [[random.randint(0, 9) for i in range(number + 1)] for j in range(number)]
start_time = time.time()
result_gauss = method_gauss(matrix, number)
with open('time_gauss.txt', 'w') as file:
    file.write(str(time.time() - start_time) + '\n')
with open('memory_gauss.txt', 'w') as file:
    file.write(str(getsizeof(result_gauss)) + '\n')
print('Ответ уравнений методом Gauss:', result_gauss)
