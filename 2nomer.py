import os
import time

os.system('cls')
print('\033[36m')

try:
    while True:
        start = 1
        stop = 24
        for i in range(13):
            with open('earth.md', 'r') as f:
                frames = [f.readlines()[start:stop]]
                for frame in frames:
                    print('\033[36m'.join(frame))
                    time.sleep(0.5)
                    os.system('cls')
            start = start + 24
            stop = stop + 24

except KeyboardInterrupt:
    print('\033[0m')
    os.system('cls')
